package com.example.examen1ercorte;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class CalculadoraActivity extends AppCompatActivity {
    private EditText txtNum1, txtNum2;
    private TextView lblNombre, lblRes;
    private Button btnSuma, btnResta, btnMult, btnDiv, btnLimpiar, btnRegresar;
    private Calculadora cal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_calculadora);

        // Inicializar Componentes
        txtNum1 = findViewById(R.id.txtNum1);
        txtNum2 = findViewById(R.id.txtNum2);
        lblNombre = findViewById(R.id.lblNombre);
        lblRes = findViewById(R.id.lblRes);
        btnSuma = findViewById(R.id.btnSuma);
        btnResta = findViewById(R.id.btnResta);
        btnMult = findViewById(R.id.btnMult);
        btnDiv = findViewById(R.id.btnDiv);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);

        // Inicializar la calculadora
        cal = new Calculadora();

        // Recibir datos del value
        String Nombre = getString(R.string.nombre);

        // Establecer los datos recibidos en los campos correspondientes
        lblNombre.setText(Nombre);

        // Codificar los botones
        btnSuma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtNum1.getText().toString().isEmpty() || txtNum2.getText().toString().isEmpty()) {
                    Toast.makeText(CalculadoraActivity.this, "Falto Capturar Informacion", Toast.LENGTH_SHORT).show();
                    return;
                }
                float n1 = Float.parseFloat(txtNum1.getText().toString());
                float n2 = Float.parseFloat(txtNum2.getText().toString());
                cal.setNum1(n1);
                cal.setNum2(n2);
                float res = cal.Suma(n1, n2);
                lblRes.setText(String.valueOf(res));
            }
        });

        btnResta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtNum1.getText().toString().isEmpty() || txtNum2.getText().toString().isEmpty()) {
                    Toast.makeText(CalculadoraActivity.this, "Falto Capturar Informacion", Toast.LENGTH_SHORT).show();
                    return;
                }
                float n1 = Float.parseFloat(txtNum1.getText().toString());
                float n2 = Float.parseFloat(txtNum2.getText().toString());
                cal.setNum1(n1);
                cal.setNum2(n2);
                float res = cal.Resta(n1, n2);
                lblRes.setText(String.valueOf(res));
            }
        });

        btnMult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtNum1.getText().toString().isEmpty() || txtNum2.getText().toString().isEmpty()) {
                    Toast.makeText(CalculadoraActivity.this, "Falto Capturar Informacion", Toast.LENGTH_SHORT).show();
                    return;
                }
                float n1 = Float.parseFloat(txtNum1.getText().toString());
                float n2 = Float.parseFloat(txtNum2.getText().toString());
                cal.setNum1(n1);
                cal.setNum2(n2);
                float res = cal.Multiplicacion(n1, n2);
                lblRes.setText(String.valueOf(res));
            }
        });

        btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtNum1.getText().toString().isEmpty() || txtNum2.getText().toString().isEmpty()) {
                    Toast.makeText(CalculadoraActivity.this, "Falto Capturar Informacion", Toast.LENGTH_SHORT).show();
                    return;
                }
                float n1 = Float.parseFloat(txtNum1.getText().toString());
                float n2 = Float.parseFloat(txtNum2.getText().toString());
                if (n2 == 0) {
                    Toast.makeText(CalculadoraActivity.this, "No se puede dividir por cero", Toast.LENGTH_SHORT).show();
                    return;
                }
                cal.setNum1(n1);
                cal.setNum2(n2);
                float res = cal.Division(n1, n2);
                lblRes.setText(String.valueOf(res));
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNum1.setText("");
                txtNum1.setHint("Ingresa el primer numero aquí");
                txtNum2.setText("");
                txtNum2.setHint("Ingresa el segundo numero aquí");
                lblRes.setText("");
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
}
