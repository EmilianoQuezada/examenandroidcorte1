package com.example.examen1ercorte;

public class Calculadora {

    private float num1, num2;

    public float getNum1() {
        return num1;
    }

    public void setNum1(float num1) {
        this.num1 = num1;
    }

    public float getNum2() {
        return num2;
    }

    public void setNum2(float num2) {
        this.num2 = num2;
    }

    public Calculadora(float num1, float num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    public Calculadora(){
        this.num1=0.0f;
        this.num2=0.0f;
    }

    public float Suma(float num1, float num2){
        float res;
        res = num1 + num2;
        return res;
    }
    public float Resta(float num1, float num2){
        float res;
        res = num1 - num2;
        return res;
    }
    public float Multiplicacion(float num1, float num2){
        float res;
        res = num1 * num2;
        return res;
    }
    public float Division(float num1, float num2){
        float res;
        res = num1 / num2;
        return res;
    }
}
